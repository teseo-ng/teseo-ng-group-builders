# Teseo Angular Group Builders

Allows the execution of commands using angular cli (ng)

## Getting Started

In your project, modify your angular json by adding the new builders:

```json
{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "my-angular-project": {
      "projectType": "application",
      "schematics": {},
      "root": "",
      "sourceRoot": "src",
      "prefix": "app",
      "architect": {
        ...

        "scaffolding": {
          "builder": "@teseo/ng-group-builders:scaffolding",
          "options": {
            "generate": [
              { "schematic": "component", "name": "modules/example-component", "options": { } },
              { "schematic": "component", "name": "modules/another-component", "options": { } }
            ]
          }
        }

        ...
```

### Prerequisites

You must have angular cli installed.

```
npm install -g @angular/cli
```

### Installing

To install:

```
npm install @teseo/ng-group-builders
```

## Authors

* **Antonio Hermosilla** - *Initial work* - [Teseo Ng](https://gitlab.com/teseo-ng)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
