const architect = require('@angular-devkit/architect');
const childProcess = require('child_process');


function processArray(arr, fn) {
  return arr.reduce(
    (p, v) => p.then((a) => fn(...v).then((r) => a.concat([r]))),
    Promise.resolve([])
  );
}

function ngCommand(command, args, logger) {
  return new Promise((resolve, reject) => {
    logger.info(` @ -> ng ${args} \n`);
    const child = childProcess.spawn(command, args, { stdio: 'pipe' });
    child.stdout.on('data', (data) => logger.info(data.toString()));
    child.stderr.on('data', (data) => logger.error(data.toString()));
    child.on('close', (code) => resolve());
  });
}

exports.default = architect.createBuilder((options, context) => {

  return new Promise((resolve /*, reject */) => {

    context.logger.info(`Execute ng-group-builders to ${context.target ? context.target.project : ''} ...`);

    const listExecute = [];
    Object.keys(options).forEach((command) => {
      const instructions = options[command];
      instructions.forEach(ins => {
        listExecute.push(['ng', [command, ins.schematic, ins.name], context.logger]);
      });
    });

    processArray(listExecute, ngCommand).then(result => {
      context.logger.info('finish');
      resolve({ success: true });
    }).catch((err) => {
      context.logger.error(`      ${err}`);
      resolve({ success: true });
    });
  });
});
